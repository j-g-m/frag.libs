import config
import ipynb_to_py
import function_scraper
import ngram_extractor as ne
import file_reconstructor
import util_functions

def main():
    """Please define the following in config.py:
            datapath: path for storing all data
            module: Importname of module to be analyzed
            dbname: name of the database with a leading /
            modul_github: to automatically remove forks 
                of the modul. Format: user/repo 
            github_user: username for api access
            github_token: token for api access
        Running this will perform all tasks.
        All tasks can be resumed or restarted if aborted.
    """
    repos = config.datapath + "/repos/"
    #Create files from csv files
    file_reconstructor.reconstruct()
    #convert jupyter notebooks to python
    ipynb_to_py.multi_convert(repos)
    #get list of of module functions
    function_scraper.create_function_list()
    print("Analyzing: " + repos)
    counter = ne.file_counter(config)
    #Count all function calls of module functions
    counter.count()
    #Delete empty folders
    util_functions.del_empty_folders(repos)
    #create ngrams:
    for i in range(4):
        gram = ne.ngram(i)
        gram.fast_create()

if __name__ == "__main__":
    main()