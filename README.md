## Potential Process

1. crawl github/gitlab for code using a specific library in a specific language (scikit?!)
2. clean code from custom variables
3. create a metric to analyze similarity
4. cluster identical/extremely similar fragments
5. create statistics on fragment usage
6. store resulting fragments in [snippet format](https://code.visualstudio.com/docs/editor/userdefinedsnippets)(?!) in sqlite database


## **Actual Process**

1. Get github data from BigQuery for $module
    1. SELECT * FROM `bigquery-public-data.github_repos.contents` WHERE content LIKE '%$module%'';
    2. SELECT `ghcrawling.$module.files`.id, `bigquery-public-data.github_repos.files`.repo_name, `bigquery-public-data.github_repos.files`.ref, `bigquery-public-data.github_repos.files`.path, `bigquery-public-data.github_repos.files`.mode, `bigquery-public-data.github_repos.files`.symlink_target, `ghcrawling.$module.files`.size, `ghcrawling.$module.files`.content,`ghcrawling.$module.files`.binary,`ghcrawling.$module.files`.copies FROM `bigquery-public-data.github_repos.files` INNER JOIN `ghcrawling.$module.files` on `bigquery-public-data.github_repos.files`.id = `ghcrawling.$module.files`.id;
    3. Export to Google Bucket
    4. Download Bucket with [GSutil](https://cloud.google.com/storage/docs/gsutil?hl=de)
    5. Reconstruct files from csvs [file_reconstructor](https://gitlab.com/j-g-m/frag.libs/blob/master/file_reconstructor.py)
    6. convert *.ipynb to *.py with: [ipynb_to_py.py](https://gitlab.com/j-g-m/frag.libs/blob/master/ipynb_to_py.py)

2. Create n-grams using [ngram_extractor.py](https://gitlab.com/j-g-m/frag.libs/blob/master/ngram_extractor.py)
    1. Remove empty folders with utility function.