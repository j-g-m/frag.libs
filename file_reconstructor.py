import pandas as pd
import config
import os
from util_functions import git_requester
import ast


def patch_together(path,cull=True):
    output = pd.DataFrame()
    for filename in os.listdir(path):
        if cull:
            if os.path.isfile(config.datapath+"/forks.csv"):
                to_drop = ast.literal_eval(open(config.datapath+"/forks.csv","r").readlines()[0])
            else:
                git_req = git_requester()
                to_drop = git_req.get_forks(config.module_github)
            if not not any(drop in filename for drop in to_drop):
                print("reading: "+filename)
                input = pd.read_csv(path+filename, compression='gzip', encoding='utf-8')
                output = output.append(input,ignore_index=True)
                print("New file list:")
                print(output)
        else:
            print("reading: "+filename)
            input = pd.read_csv(path+filename, compression='gzip', encoding='utf-8')
            output = output.append(input,ignore_index=True)
            print("New file list:")
            print(output)
    return output
 

def reconstruct():
    files = patch_together(config.datapath+"/results/")
    for index, row in files.iterrows():
        if row["path"].endswith(".py") or row["path"].endswith(".ipynb"):
            if len(row["path"].rsplit("/",1))<2:
                path=""
            else:
                path=row["path"].rsplit("/",1)[0]
            if not os.path.isdir(config.datapath+"/repos/"+row["repo_name"]+"/"+path):
                os.makedirs(config.datapath+"/repos/"+row["repo_name"]+"/"+path)
            f = open(config.datapath+"/repos/"+row["repo_name"]+"/"+row["path"],"w")
            f.write(row["content"])
