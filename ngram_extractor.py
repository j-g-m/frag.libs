import ast
import json
import imp
import os
import config
import sys
from collections import OrderedDict
from collections import Counter
from itertools import combinations
import pandas as pd
from multiprocessing import Process, Manager
import numpy as np
from util_functions import git_requester 
import util_functions


def analyze(file, module, depth=0):
    result={}
    if depth<5:
        try:
            with open(file, "r") as source:
                tree = ast.parse(source.read())
        except FileNotFoundError:
            with open(file + ".py", "r") as source:
                tree = ast.parse(source.read())
        except IndentationError:
            print("File not properly indented.")
            return {}
        except SyntaxError:
            print("Improper syntax.")
            return {}
        except UnicodeDecodeError:
            print("Decode error.")
            return {}
        except ValueError:
            print("Weird stuff is going on.")
            return {}
        sys.path.append(os.path.abspath(file).rsplit('/',1)[0])
        identifier = Identifier(module,file, depth)
        try:
            identifier.visit(tree)
        except RecursionError:
            print("Too many recursions")
            return identifier.functions
        sys.path.remove(os.path.abspath(file).rsplit('/',1)[0])
        #replace None with key and remove leading dots.
        for key,values in identifier.functions.items():
            if values == None:
                identifier.functions[key] = [key]
            else:
                i=0
                for item in values: 
                    if item!=None:
                        if item[0] == '.':
                            values[i]=item[1:]
                        i+=1
        result= identifier.functions
    return result


class Identifier(ast.NodeVisitor):
    def __init__(self,module,file,depth):
        """Initialize some variables open a connection to DB and create the func_freq table if it doesn't exist yet."""
        self.filename = file
        self.module = module
        self.functions = {}
        self.depth = depth

        #load the module function list.
        with open(config.module+'_functions.txt', 'r') as json_file:  
            data = json.load(json_file)

        #functions with identical names (case insensitive) are merged
        self.module_functions = set([])
        for item in data:
                self.module_functions.add(item.lower())

        column_str = ""
        for item in self.module_functions:
            column_str+= " \"" + item + "\" int,"
        #Create a table consisting of columns: filename, function-list-entries.


    def visit_Import(self, node):
        for alias in node.names:
            #check if module is installed
            try:   
                imp.find_module(alias.name.split('.', 1 )[0])[1]
            except:
                if self.module in alias.name:
                    if alias.name in self.functions:
                        self.functions[alias.name].append(alias.name)
                    else:
                        self.functions[alias.name] = [alias.name]
            else:
                #if it is check whether it is local
                mod_path=imp.find_module(alias.name.split('.', 1 )[0])[1]
                if mod_path != None:
                    if (mod_path.startswith(config.datapath)):
                        try:
                            for key, val in analyze(os.path.abspath(self.filename).rsplit('/',1)[0]+"/"+alias.name + ".py",config.module,self.depth
                            +1).items():
                                if key in self.functions:
                                    self.functions[key] += [alias.name+"."+s for s in val]
                                else:
                                    self.functions[key] = [alias.name+"."+ s for s in val]
                        except FileNotFoundError:
                            print("Illegal import")
                    else:
                        #if not append it's name
                        if self.module in alias.name:
                            if alias.asname:
                                if alias.name in self.functions:
                                    self.functions[alias.name].append(alias.asname)
                                else:
                                    if alias.asname!=None:
                                        self.functions[alias.name] = [alias.asname]
                            else:
                                if alias.name in self.functions:
                                    self.functions[alias.name].append(alias.name)
                                else:
                                    self.functions[alias.name] = [alias.name]
        self.generic_visit(node)


    def visit_ImportFrom(self, node):
        try:   
            imp.find_module(node.module.split('.', 1 )[0])[1]
        except:
            ""
        else:
            #if it is check whether it is local
            mod_path=imp.find_module(node.module.split('.', 1 )[0])[1]
            if mod_path != None:
                if not mod_path.startswith(config.datapath):
                    for item in node.names:
                        if self.module in node.module:
                            if item.asname==None:
                                if node.module+'.'+item.name in self.functions:
                                    self.functions[node.module+'.'+item.name].append(item.name)
                                else:
                                    self.functions[node.module+'.'+item.name] = [item.name]
                            else:
                                if node.module+'.'+item.name in self.functions:
                                    self.functions[node.module+'.'+item.name].append(item.asname)
                                else:
                                    self.functions[node.module+'.'+item.name] = [item.asname]
        self.generic_visit(node)


class file_counter():


    def __init__(self,config_in=config):
        self.config = config_in
        self.function_counter = OrderedDict()
        #load the module function list.
        with open(config_in.module+"_functions.txt", 'r') as f:
             self.module_functions = json.load(f)
        #functions with identical names (case insensitive) are merged
        column_str = "filename, "
        for item in self.module_functions:
            self.function_counter[item]=0
            column_str+= item + ","
        
        #Create a table consisting of columns: filename, function-list-entries.
        if not os.path.isfile(config_in.datapath+"/function_counter.csv"):
            self.functions_file = open(config_in.datapath+"/function_counter.csv","w")
            self.functions_file.write(column_str[:-2]+"\n")
            self.functions_file.close()
        if not os.path.isfile(config_in.datapath+"/file_list.csv"):
            temp = open(config_in.datapath+"/file_list.csv","w")
            temp.close()
        input = open(config_in.datapath+"/file_list.csv","r")
        self.file_list = input.readlines()
        input.close()
        self.file_list_output = open(config_in.datapath+"/file_list.csv","a+")
        self.functions_file = open(config_in.datapath+"/function_counter.csv","a+")
        

    def count(self):
        """Go file by file, count the module-functions in the file and write ngram to db."""
        old_stuff = True
        for root, dirs, files in os.walk(self.config.datapath):
            for name in sorted(files):
                if name.endswith((".py")):
                    filename=(root + "/" + name)
                    #Check if file already analyzed
                    """IMPORTANT: This only works correctly, if the underlying file system returns the dirs in a sorted order!"""
                    if old_stuff and self.file_list != []:
                        found = False
                        #check if filename in function_list
                        for line in self.file_list:
                            if filename.replace(",","²") in line:
                                found = True                        
                        if not found:
                            old_stuff = False
                            #if file not in list analyze it.
                            self.__analyze(filename)
                    #else analyze it
                    else:
                        self.__analyze(filename)
                        
                        
    def __analyze(self,filename):
        self.function_counter = {}
        aliases = analyze(filename,config.module)
        function_dict = {}
        if aliases != {}:
            #Create dictionary of all possible ways to call a function from self.config.module
            for item in self.module_functions:
                self.function_counter[item]=0
                function_dict[item]=[]
                for key,val in aliases.items():
                    if key == config.module:
                        for value in val:
                            function_dict[item].append(value + "." + item)
                    else:
                        temp = key[len(config.module)+1:]
                        if temp in item:
                            for value in val:
                                if item[len(temp)+1:]!="":
                                    function_dict[item].append(value + '.' + item[len(temp)+1:])
                                else:
                                    function_dict[item].append(value)
            #if not create entry in db
            f = open(filename, "r")
            try:
                fileslines = f.readlines()
            except UnicodeDecodeError:
                print("Decode error.")
                return
            for line in fileslines:
                self.__check_line(line,function_dict)
            f.close()
            
            #save function_counter to database:
            if self.function_counter != {}:
                output = filename.replace(",","²")+", "
                for key,val in self.function_counter.items():
                    if val > 0:
                        print(key+": "+str(val))
                    output += str(val)+", "
                self.functions_file.write(output[:-2]+"\n")
                self.file_list_output.write(filename.replace(",","²")+"\n")
                print(filename + " has been analyzed and results are stored.")
                return
            #clear function_counter and remove uninteresting files:
        else:
            print("nothing found in " + filename)
            print("deleting " + filename)
            os.remove(filename)
        self.function_counter = {}


    def __check_line(self, line, functions):
        """Go line by line and count which function appears how often."""
        for function,aliases in functions.items():
            for alias in aliases:
                if alias in line: 
                    self.function_counter[function] +=1

def remap_keys(mapping):
    output = OrderedDict()
    for k, v in mapping.items():
        output[str(k)[1:-1]] = v
    return output


class ngram:

    def __init__(self,n, config_in=config, cull=True):

        self.n = n
        self.gram = {}
        self.gram_list=[]
        self.fc_columns = []
        self.cull = cull
        self.config = config_in

    def _str__(self):
        print(self.gram)

    def export(self):
        output = remap_keys(self.gram)
        ext="_gram"
        if self.cull:
            ext+="_culled"
        print("writing to: " + self.config.datapath + "/" + str(self.n) + ext + '.json')
        with open(self.config.datapath+"/"+str(self.n)+ ext + '.json', 'w') as outfile:  
            json.dump(output, outfile, indent=4)

    def __create(self,rows,gram_list,ident):
        """Creates ngrams from the function counts in the db."""
        print(str(ident)+". Thread, Number of Rows:"+str(len(rows)))
        for row in rows:
            row_list=[]
            for i in np.nonzero(row):
                row_list.append(self.fc_columns[i])
            row_list=(np.array(row_list).tolist()[0])
            gram_list+=[sorted(list(tup)) for tup in list(combinations(row_list,self.n))]#
        print(str(ident)+". Thread: done")

    def __read_csv(self,chunksize=50000):
        """Reads a large csv file and culls forks of the module if asked to."""
        fc = pd.DataFrame()
        if self.cull:
            if os.path.isfile(self.config.datapath+"/forks.csv"):
                to_drop = ast.literal_eval(open(self.config.datapath+"/forks.csv","r").readlines()[0])
            else:
                git_req = git_requester()
                to_drop = git_req.get_forks(self.config.module_github)
        for chunk in pd.read_csv(self.config.datapath+"/function_counter.csv", chunksize=chunksize ,index_col=0,iterator=True):
            if self.cull:
                chunk = chunk[~chunk.index.str.contains('|'.join(to_drop))]
            fc = pd.concat([fc,chunk])
        return fc

    def fast_create(self):
        """Creates"""
        print("Reading Function_counter")
        fc=self.__read_csv()
        if self.n>1:
            v = fc.values
            m = (v[:, :-1] != 0).sum(1) > self.n-1
            fc = pd.DataFrame(v[m], fc.index[m], fc.columns)
            print("Function_counter has been read")
            #drop columns with no values > 0
            fc = fc.loc[:, (fc != 0).any(axis=0)]
            self.fc_columns = fc.columns
            gram_list = util_functions.multithread_shared_object(self.__create,"list",fc.values.tolist())
            self.gram = Counter(tuple(item) for item in gram_list)
            self.gram = OrderedDict(sorted(self.gram.items(), key=lambda kv: kv[1],reverse=True))
        else:
            fc.loc["Total"] =  fc.sum(axis=0)
            self.gram=OrderedDict(sorted(fc.loc["Total"].to_dict().items(), key=lambda kv: kv[1],reverse=True))

        self.export()

