import config
import subprocess
import wget
from platform import system
import tarfile
import xml.etree.ElementTree as ET
import os
import shutil


class code:
    def __init__(self, file, start,end,pcid):
        self.file = file
        self.start = start
        self.end = end
        self.pcid = pcid

class frag:
    def __init__(self,id_F,nclones,length,sim):
        self.id = id_F
        self.nclones = nclones
        self.len = length
        self.sim = sim
        self.sources = []

    def set_sources(self, sources):
        self.sources = sources

    def add_source(self,source):
        self.sources.append(source)

    def __str__(self):
        return "id: " + self.id + " multiplicity: " + self.nclones + " length: " + self.len + " similiarity: " + self.sim

def install_nicad():
    """Installs txl and nicad"""
    install = ''
    if config.nicad == None:
        while install not in ['y','n']:
            install = input('Is NiCad installed? [y/n] ').lower()
        if (install =='n'):
            txl = ''
            while txl not in ['y','n']:
                txl = input('Is txl already installed? [y/n] ').lower()
            if (txl== 'n'):
                txl_linux = 'https://www.txl.ca/download/4923-txl10.7.linux64.tar.gz'
                txl_mac = 'https://www.txl.ca/download/5012-txl10.7.macosx64.tar.gz'
                txl_win_vs = 'https://www.txl.ca/download/14648-Txl107win64.zip'
                txl_win_cy = 'https://www.txl.ca/download/21327-txl10.7.cygwin64.tar.gz'
                txl_win_my = 'https://www.txl.ca/download/21727-txl10.7.mingw64.tar.gz'
                if system() == 'Linux':
                    txl_tar = wget.download(txl_linux)
                if system() == 'Darwin':
                    txl_tar = wget.download(txl_mac) 
                if system() == 'Windows':
                    cmd = ''
                    while cmd not in ['VS','CY','MS']:
                        cmd = input('Are you using VS2017, Cygwin, MySys2? [VS/Cy,My] ').upper()
                    if (cmd == 'VS'):
                        txl_tar = wget.download(txl_win_vs)
                    elif (cmd == 'CY'):
                        txl_tar = wget.download(txl_win_cy)
                    elif (cmd == 'MS'):
                        txl_tar = wget.download(txl_win_my)
                tar = tarfile.open(txl_tar)
                folder = os.path.commonprefix(tar.getnames())
                tar.extractall()
                tar.close()
                os.remove(txl_tar)
                #subprocess.check_call(['./' + folder + '/InstallTxl'])
                shutil.rmtree(folder)
            nicad = "https://www.txl.ca/download/5949-NiCad-5.1.tar.gz"
            nicad_tar = wget.download(nicad)
            tar = tarfile.open(nicad_tar)
            nicad_folder = os.path.commonprefix(tar.getnames())
            tar.extractall()
            tar.close()
            os.remove(nicad_tar)
            subprocess.check_call(["make","-C" ,  nicad_folder])
            
            print("Done.")
        
    

def run_nicad():
    subprocess.check_call([str(config.nicad),'functions','py',str(config.datapath)+'/repos', 'type3-2'])


def extract_frag():
    """Extracts source from fragment data, and creates abstract fragment from concrete code instances."""
    frags = read_xml()
    #todo

def read_xml():
    """Reads the output of the NiCad report and saves it in the fragment class"""
    tree = ET.parse(config.datapath + '/repos_functions-blind-clones/repos_functions-blind-clones-0.30-classes.xml')
    root = tree.getroot()
    frags = []
    for fragment in root.iter('class'):
        i = 0
        frags.append(frag(fragment.get('classid'),fragment.get('nclones'),fragment.get('nlines'),fragment.get('similarity')))
        for source in fragment.findall('source'):
            frags[i].add_source(code(source.get('file'),source.get('startline'),source.get('endline'),source.get('pcid')))
        i+=1
    return frags

install_nicad()
run_nicad()