import config 
import os
from multiprocessing import Pool,Process,Manager
import numpy as np
import requests
from requests.auth import HTTPBasicAuth
from time import time,sleep

def multithread(function, param,not_util=2):
    """Run the function with parameter param on multiple cores."""
    #multiprocessing
    if len(os.sched_getaffinity(0)) > 2:
        cpus = len(os.sched_getaffinity(0))-not_util
    else:
        cpus = len(os.sched_getaffinity(0))
    with Pool(cpus) as p:
        p.map(function,param)

def multithread_shared_object(function,s_type, iterable,arguments=None,not_util=2):
    """Hand a shared object of s_type and an iterable to a function be processed in parallel."""
    manager = Manager()
    #assign shared resource
    if s_type == "list":
        shared = manager.list()
    if s_type == "dict":
        shared = manager.dict()
    #if threads > 2 reserve the number specified in not_util, use the rest
    if len(os.sched_getaffinity(0)) > 2:
        cpus = len(os.sched_getaffinity(0))-not_util
    else:
        cpus = len(os.sched_getaffinity(0))
    processes = []
    #split iterable into parts
    split_iter = np.array_split(np.array(iterable),cpus)
    #create process, start and join them
    for i in range(cpus):
        if arguments:
            p=Process(target=function, args=([split_iter[i],shared,arguments,i]))
        else:
            p=Process(target=function, args=([split_iter[i],shared,i]))
        processes.append(p)
    for p in processes:
        p.start()
    for p in processes:
        p.join()
    return shared

def del_empty_folders(path, removeRoot=True):
  """Function to remove empty folders"""
  if not os.path.isdir(path):
    return

  # remove empty subfolders
  files = os.listdir(path)
  if len(files):
    for f in files:
      fullpath = os.path.join(path, f)
      if os.path.isdir(fullpath):
        del_empty_folders(fullpath)

  # if folder empty, delete it
  files = os.listdir(path)
  if len(files) == 0 and removeRoot:
    print("Removing empty folder:", path)
    try:
      os.system('rmdir "%s"' % path)
    except:
      print("error")


class git_requester:
    def __init__(self):
        self.auth = HTTPBasicAuth(config.github_user, config.github_token)

    def login(self):
        requests.get("https://api.github.com/user", auth=self.auth)

    def rate_limit(self, request):
        #nud3l/github-search-crawler
        limit = requests.get('https://api.github.com/rate_limit', auth=self.auth)
        limit_json = limit.json()

        if request is 'search':
            remaining_search = limit_json["resources"]["search"]["remaining"]
            reset_time = limit_json["resources"]["search"]["reset"]
            if remaining_search is 0:
                # wait until we can do search requests again
                sleep_time = reset_time - int(time())
                print("Search limit reached. Waiting {} seconds".format(sleep_time))
                sleep(sleep_time)
        elif request is 'core':
            remaining_download = limit_json["resources"]["core"]["remaining"]
            reset_time = limit_json["resources"]["core"]["reset"]

            if remaining_download is 0:
                # wait until we can request again
                sleep_time = reset_time - int(time())
                print("Core limit is reached. Waiting {} seconds".format(sleep_time))
                sleep(sleep_time)

    def get_forks(self, module_github):
        self.login()
        i = 0
        response = []
        done = False
        repo_list = []
        while(not done):
            response = requests.get("https://api.github.com/repos/"+module_github+"/forks"+'?page='+str(i), auth=self.auth)
            self.rate_limit(request='core')
            if response.json() == []:
                print("done")
                done = True
            if response.status_code is 200:
                print("Page: " + str(i))
                for item in response.json():
                    repo_list.append(item["full_name"])
            i += 1
        return repo_list