import config
import os
from multiprocessing import Pool
import util_functions


def convert(path):
    """Convert .ipynb to .py"""
    for root, dirs, files in os.walk(path):
        for name in files:
            if name.endswith((".ipynb")):
                print("Converting: " + root + '/' + name)
                os.system('jupyter nbconvert --to python ' +'\'' +os.path.join( root, name)+'\'')
                print('Deleting: ' + root + '/' + name )
                os.remove( os.path.join( root, name ) )
#sometime i get: Notebook JSON is invalid: 'execution_count' is a required property so some files are not converted and just deleted (maybe 1/1000)

def multi_convert(path, not_util=2):
    """Run the converter on multiple cores."""
    #multiprocessing (cuts down the processing time by a factor of ~4)
    dirlist=[]
    #first create a list of all directories with ipynb files 
    for root, dirs, files in os.walk(path):
        for name in files:
            if name.endswith((".ipynb")):
                dirlist.append(root)
    #if there are more than two threads available use number of threads -2 to leave the pc some cores to do other stuff
    util_functions.multithread(convert,set(dirlist))