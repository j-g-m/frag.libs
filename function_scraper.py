import os
import imp
import ast
import config
import json


def analyze(path,root,filename):
    function_list=[]
    with open(filename) as file:
        node = ast.parse(file.read())

    functions = [n for n in node.body if isinstance(n, ast.FunctionDef)]
    classes = [n for n in node.body if isinstance(n, ast.ClassDef)]
    variables = [n for n in node.body if isinstance(n, ast.Assign)]
    for nodes in variables:
        for target in nodes.targets:
            if type(target)==ast.Name:
                if target.id=="__all__":
                    for item in nodes.value.elts:
                        function_name = filename.replace("__init__.py","").replace(path,"").replace("/",".")[1:]+item.s
                        function_list.append(function_name)
                        if "__init__" in function_name:
                            function_list.append(function_name.replace("__init__.",""))
                        

    for function in functions:
        function_name = filename.replace(".py","").replace(path,"").replace("/",".")[1:]+"."+function.name
        function_list.append(function_name)
        if "__init__" in function_name:
            function_list.append(function_name.replace("__init__.",""))

    for class_ in classes:
        methods = [n for n in class_.body if isinstance(n, ast.FunctionDef)]
        for method in methods:
            function_name=filename.replace(".py","").replace(path,"").replace("/",".")[1:]+"."+class_.name
            function_list.append(function_name)
            if "__init__" in function_name:
                function_list.append(function_name.replace("__init__.",""))
    return function_list

def create_function_list():
    path=imp.find_module(config.module)[1]
    function_list=[]
    for root, dirs, files in os.walk(imp.find_module(config.module)[1]):
        for name in files:
            if name.endswith((".py")):
                function_list+=analyze(path,root,root+"/"+name)

    with open(config.module + '_functions.txt', 'w') as outfile:  
        json.dump(sorted(list(set(function_list))), outfile)
